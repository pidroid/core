//
// Created by peter on 2019-08-15.
//

#ifndef CORE_VISION_HPP
#define CORE_VISION_HPP

class Vision {
public:
	Vision();
	~Vision();

	int Detect(const char* imageFile);
};


#endif //CORE_VISION_HPP
