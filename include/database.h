//
// Created by peter on 2019-08-14.
//

#ifndef CORE_DATABASE_H
#define CORE_DATABASE_H

#include <sqlite3.h>

int loadOrSaveDb(sqlite3 *pInMemory, const char *zFilename, int isSave);

#endif //CORE_DATABASE_H
