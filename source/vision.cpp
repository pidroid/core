//
// Created by peter on 2019-08-15.
//
// see https://opencvexamples.blogspot.com/2013/10/face-detection-using-haar-cascade.html

#include "../include/vision.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

const char* CASCADE = "/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml";

Vision::Vision() {

}

Vision::~Vision() {

}

int Vision::Detect(const char *imageFile) {
	Mat image;
	image = imread(imageFile, CV_LOAD_IMAGE_COLOR);

	CascadeClassifier face_cascade;
	face_cascade.load(CASCADE);

	std::vector<Rect> faces;
	face_cascade.detectMultiScale( image, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30) );

	for( int i = 0; i < faces.size(); i++ ) {
		printf("Face %d: w=%d h=%d x=%d y=%d\n", i, faces[i].width, faces[i].height, faces[i].x, faces[i].y);
	}

	return 0;
}
