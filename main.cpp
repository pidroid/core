#include <stdio.h>
#include <sys/stat.h>
#include <bits/stdc++.h>
#include <sqlite3.h>
#include "include/database.h"
#include "include/vision.hpp"
using namespace std;

void setupDirectories() {
	int err = 0;
	__mode_t mode = S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH;

	vector <string> dirs;

	dirs.push_back("./db");
	dirs.push_back("./log");

	for (int i = 0; i < dirs.size(); i++) {
		err = mkdir(dirs[i].c_str(), mode);
		if (err == -1) {
			printf("Direction creation FAILED:    %s\n", dirs[i].c_str());
		} else {
			printf("Direction creation SUCCEEDED: %s\n", dirs[i].c_str());
		}
	}
}

Vision* setupVision() {
	Vision* v = new Vision();
	return v;
}

void setupDatabase() {
	sqlite3 *db;
	int rc;

	rc = sqlite3_open(":memory:", &db);

	int result = loadOrSaveDb(db, "save.db", 1);
	printf("save result: %d\n", result);

	sqlite3_close(db);
}

int main(int argc, char **argv) {
	//setupDirectories();

	Vision* v = setupVision();
	v->Detect("/home/peter/Downloads/astro.jpg");

	//setupDatabase();

	return 0;
}
